#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* doSth(void *args) {
   for (int i = 1; i <= 10; ++i) {
        printf("String %d in thread 2\n", i);
    }
    return 0;
}
 
int main() {
    pthread_t thread;
    int status_addr;
    
    int status = pthread_create(&thread, NULL, doSth, NULL);
    if (status != 0) {
        printf("Error: can't create thread, status = %d\n", status);
        return -1;
    }
    
    status = pthread_join(thread, (void**)&status_addr);
    if (status != 0) {
        printf("Error: can't join thread, status = %d\n", status);
        exit(-1);
    }
    
    for (int i = 1; i <= 10; ++i) {
        printf("String %d in thread 1\n", i);
    }
    return 0;
}
 
