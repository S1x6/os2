#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void onCancel(void *args) {
    printf("I'm canceling\n");
}

void* doSth(void *args) {
    pthread_cleanup_push(onCancel, NULL);
    for(;;)
        write(0, "String in thread 2\n", 19);
    pthread_cleanup_pop(0);
    return NULL;
}
 
int main() {
    pthread_t thread;
    
    int status = pthread_create(&thread, NULL, doSth, NULL);
    if (status != 0) {
        printf("Error: can't create thread, status = %d\n", status);
        return -1;
    }
    
    sleep(2);
    if (pthread_cancel(thread) != 0) {
        printf("error canceling thread");
    }
    pthread_join(thread, NULL);
    return 0;
}
