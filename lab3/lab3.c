#include "pthread.h"
#include "stdio.h"
#include "malloc.h"
#include "string.h"

void* routine(void* arg) {
    printf("%s", (char*)arg);
    free((char*)arg);
    return NULL;
}

int main() {
    
    char* arg1 = malloc(sizeof(char) * 4);
    char* arg2 = malloc(sizeof(char) * 4);
    char* arg3 = malloc(sizeof(char) * 4);
    char* arg4 = malloc(sizeof(char) * 4);
    memset(arg1, 'a', 4);
    memset(arg2, 'b', 4);
    memset(arg3, 'c', 4);
    memset(arg4, 'd', 4);
    
    pthread_t th1, th2, th3, th4; 

    if (pthread_create(&th1, NULL, routine, arg1) != 0) {
        printf("Error: can't create thread 1");
        return -1;
    }
    if (pthread_create(&th2, NULL, routine, arg2) != 0) {
        printf("Error: can't create thread 2");
        return -1;
    }
    if (pthread_create(&th3, NULL, routine, arg3) != 0) {
        printf("Error: can't create thread 3");
        return -1;
    }
    if (pthread_create(&th4, NULL, routine, arg4) != 0) {
        printf("Error: can't create thread 4");
        return -1;
    }


    pthread_exit(0);
}
