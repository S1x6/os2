#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <malloc.h>
#include <errno.h>
#define num_steps 8

typedef struct data_ {
    int step;
    int index;
} info;

void * routine(void * args) {
    double *pi = (double*)calloc(1, sizeof(double));
    int i;
    info data = *(info*)args;
    long max = data.step * (data.index+1);
    printf("%ld\n", max);
    for (i = data.step*data.index; i < max; i++) {
         *pi += 1.0/(i*4.0 + 1.0);
         *pi -= 1.0/(i*4.0 + 3.0);
    }
    *pi *= 4.0;
    printf("%f\n", *pi);
    return (void*)pi;
}

void * pointer_error_handler(void *p, const char *text) {
    if (p == NULL) {
	int code = errno;
	perror(text);
	exit(code);
    }
}
void * error_handler(int retCode, const char *text) {
    if (retCode != 0) {
	int code = errno;
	perror(text);
	exit(code);
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("need one arg: number of threads");
    }
    int threads = atoi(argv[1]);
    double *partial_sum;
    double pi = 0;
    info *data = (info *)malloc(sizeof(info) * threads);
    pointer_error_handler(data, "malloc");
    for (int i = 0; i < threads; ++i) {
        data[i].step = num_steps / threads;
        data[i].index = i;
    }
    pthread_t *ths = (pthread_t *)malloc(sizeof(pthread_t) * threads);
    long arg = num_steps / threads;
    for (int i = 0; i < threads; ++i) {
        error_handler(pthread_create(&ths[i], NULL, routine, (void *)&data[i]), "pthread_create");
    }
    
    for (int i = 0; i < threads; ++i) {
	error_handler(pthread_join(ths[i], (void**)&partial_sum), "pthread_join");
        pi += *((double*)partial_sum);
        free((double*)partial_sum);
    }
    printf("pi done - %.15g \n", pi);  
    free(data);
    free(ths);
}

