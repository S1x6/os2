#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

void* doSth(void *args) {
   while (1) {
        printf("String in thread 2\n");
    }
    return 0;
}
 
int main() {
    pthread_t thread;
    
    int status = pthread_create(&thread, NULL, doSth, NULL);
    if (status != 0) {
        printf("Error: can't create thread, status = %d\n", status);
        return -1;
    }
    
    sleep(2);
    if (pthread_cancel(thread) != 0) {
        printf("error canceling thread");
    }
    
    return 0;
}
 
