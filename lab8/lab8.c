#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <string.h>

int stop = 0;

typedef struct data_ {
    int total;
    int index;
} info;

void hdl(int sig)
{
	stop = 1;
}

void * routine(void * args) {
    double *pi = (double*)calloc(1, sizeof(double));
    int i;
    info data = *(info*)args;
    
    i = data.index;
    while (stop == 0) {
         *pi += 1.0/(i*4.0 + 1.0);
         *pi -= 1.0/(i*4.0 + 3.0);
          i += data.total;
    }
    *pi *= 4.0;
    printf("%.15g\n", *pi);
    return (void*)pi;
}

void * pointer_error_handler(void *p, const char *text) {
    if (p == NULL) {
	int code = errno;
	perror(text);
	exit(code);
    }
}
void * error_handler(int retCode, const char *text) {
    if (retCode != 0) {
	int code = errno;
	perror(text);
	exit(code);
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("need one arg: number of threads");
    }
    
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = hdl;
    sigset_t set; 
    sigemptyset(&set);                                                             
    sigaddset(&set, SIGINT); 
    act.sa_mask = set;
    sigaction(SIGINT, &act, 0);
    
    int threads = atoi(argv[1]);
    double *partial_sum;
    double pi = 0;
    info *data = (info *)malloc(sizeof(info) * threads);
    pointer_error_handler(data, "malloc");
    for (int i = 0; i < threads; ++i) {
        data[i].total = threads;
        data[i].index = i;
    }
    pthread_t *ths = (pthread_t *)malloc(sizeof(pthread_t) * threads);
    for (int i = 0; i < threads; ++i) {
        error_handler(pthread_create(&ths[i], NULL, routine, (void *)&data[i]), "pthread_create");
    }
    
    for (int i = 0; i < threads; ++i) {
	error_handler(pthread_join(ths[i], (void**)&partial_sum), "pthread_join");
        pi += *((double*)partial_sum);
        free((double*)partial_sum);
    }
    printf("pi done - %.15g\n", pi);  
    free(data);
    free(ths);
}
